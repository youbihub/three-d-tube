use std::f32::consts::PI;

use nalgebra::{Isometry3, Translation3, UnitQuaternion, Vector2};
use three_d::*;
use three_d_tube::Tube;

fn make_square(c: f32) -> Vec<Vector2<f32>> {
    vec![
        c * Vector2::new(0., 0.),
        c * Vector2::new(1., 0.),
        c * Vector2::new(1., 1.),
        c * Vector2::new(0., 1.),
    ]
}

fn make_ellipse(n: i32) -> Vec<Vector2<f32>> {
    (0..n)
        .map(|i| (i as f32) / (n as f32) * 2.0 * PI)
        .map(|a| Vector2::<f32>::new(0.8 * a.cos(), 0.3 * a.sin()))
        .collect()
}

fn make_transforms(n: i32, height: f32) -> Vec<Isometry3<f32>> {
    (0..n)
        .map(|i| i as f32 / n as f32 * height)
        .map(|h| {
            let a = h * std::f32::consts::PI;
            let translation = Translation3::new(0_f32, 0_f32, h);
            let rotation = UnitQuaternion::from_scaled_axis(nalgebra::Vector3::z() * a);
            Isometry3::from_parts(translation, rotation)
        })
        .collect()
}
fn make_repetitions() -> Vec<Mat4> {
    let mut l = Mat4::identity();
    l.w.x = -1.;
    let mut r = Mat4::identity();
    r.w.x = 1.;
    vec![l, r]
}

fn main() {
    let n_t = 2320; //1018 is the limit for shader with 40p ellipse otherwise ShaderLink("error: Too many vertex shader default uniform block components\n")
    let window = Window::new(WindowSettings {
        title: "Rails!".to_string(),
        max_size: Some((1280, 720)),
        ..Default::default()
    })
    .unwrap();
    let context = window.gl();
    let mut camera = Camera::new_perspective(
        window.viewport(),
        vec3(22.0, 3.0, -8.0),
        vec3(22.75, 3.0, -7.4),
        vec3(0.0, 0.0, 1.0),
        degrees(45.0),
        0.1,
        1000.0,
    );
    camera.set_view(
        Vector3 {
            x: 4.0,
            y: 2.0,
            z: 2.5,
        },
        Vector3::zero(),
        Vector3::unit_z(),
    );
    let mut control = OrbitControl::new(*camera.target(), 1.0, 100.0);
    let contour = make_ellipse(40);
    let mut tube = Gm::new(
        Tube::new(
            &context,
            &contour,
            &make_repetitions(),
            n_t,
            &make_transforms(n_t as i32, 1.0),
        ),
        PhysicalMaterial::new_transparent(
            &context,
            &CpuMaterial {
                albedo: Color {
                    r: 0,
                    g: 255,
                    b: 0,
                    a: 200,
                },
                ..Default::default()
            },
        ),
    );

    let light0 = DirectionalLight::new(&context, 1.0, Color::WHITE, &vec3(-1.0, 0.0, 0.0));
    let axes = Axes::new(&context, 0.1, 2.0);
    window.render_loop(move |mut frame_input| {
        let tfs = make_transforms(
            n_t as i32,
            (frame_input.accumulated_time as f32 / 1000. * 2. * std::f32::consts::PI / 5.).cos(),
        );
        tube.set_transforms(tfs.as_slice());
        camera.set_viewport(frame_input.viewport);
        control.handle_events(&mut camera, &mut frame_input.events);

        frame_input
            .screen()
            .clear(ClearState::color_and_depth(0.8, 0.8, 0.8, 1.0, 1.0))
            .render(&camera, tube.into_iter().chain(&axes), &[&light0]);
        FrameOutput::default()
    });
}
