// todo: use quaternion for space efficiency

uniform mat4 viewProjection;

uniform vec4 surfaceColor;
uniform vec3 slice_ts[T];
uniform vec4 slice_rs[T];
uniform mat4 repetitions[R];
uniform vec2 contour[C];

out vec3 pos;
out vec3 nor;
out vec4 col;

in float idx_cont;
in float idx_slice;
in float idx_rep;

int idx_cont_i=int(idx_cont);
int idx_slice_i=int(idx_slice);
int idx_rep_i=int(idx_rep);

vec3 rotateVec3WithQuat(vec4 q, vec3 v) {
  vec3 u = cross(q.xyz, v);
  vec3 w = q.w * u + cross(q.xyz, u);
  return v + 2.0 * w;
}

vec3 Pa(vec4 Qab, vec3 Tab, vec3 Pb){
    return rotateVec3WithQuat(Qab,Pb)+Tab;
}

void main()
{
    vec3 p0 = (repetitions[idx_rep_i]*vec4(contour[idx_cont_i],0,1)).xyz;
    // pos = slice_rs[idx_slice_i]*p0+slice_ts[idx_slice_i];
    pos = Pa(slice_rs[idx_slice_i],slice_ts[idx_slice_i],p0);
    gl_Position = viewProjection * vec4(pos,1);
    // uvs = pos.xz;
    col = vec4(1.0);

    //////////////////////
    // computing normal //
    //////////////////////
    int idx_slice_i2=(idx_slice_i!=T-1)?idx_slice_i+1:idx_slice_i-1;
    int idx_cont_i1=(idx_cont_i!=C-1)?idx_cont_i+1:0;

    vec3 q1 = (repetitions[idx_rep_i]*vec4(contour[idx_cont_i1],0,1)).xyz;
    vec3 q2 = (repetitions[idx_rep_i]*vec4(contour[idx_cont_i],0,1)).xyz;
    // vec3 p1 = slice_rs[idx_slice_i]  * q1 + slice_ts[idx_slice_i ];
    vec3 p1 = Pa(slice_rs[idx_slice_i],slice_ts[idx_slice_i ],q1);
    // vec3 p2 = slice_rs[idx_slice_i2] * q2 + slice_ts[idx_slice_i2];
    vec3 p2 = Pa(slice_rs[idx_slice_i2],slice_ts[idx_slice_i2],q2);
    vec3 u = p1-pos;
    vec3 v = (idx_slice_i!=T-1)?p2-pos:pos-p2;
    nor = cross(u,v);
    nor = normalize(nor);

}
