extern crate nalgebra as na;
use na::{Isometry3, Vector2};
use std::sync::Arc;
use three_d::{
    vec2, vec3, vec4, Camera, Context, ElementBuffer, Geometry, Mat4, Program, RenderStates,
    VertexBuffer,
};
#[derive(Clone)]

pub struct Tube {
    context: Context,
    contour: Vec<Vector2<f32>>, //todo: might not be needed to store
    repetitions: Vec<Mat4>,
    transforms: Vec<Isometry3<f32>>,
    idx_triangle: Arc<ElementBuffer>,
    idx_contour: Arc<VertexBuffer>, //todo: arc?
    idx_slice: Arc<VertexBuffer>,   //todo: arc?
    idx_rep: Arc<VertexBuffer>,     //todo: arc?
    pub first_frame: bool,
    pub change: bool,
}
impl Tube {
    pub fn new(
        context: &Context,
        contour: &Vec<Vector2<f32>>,
        rep: &[Mat4],
        nslice: usize,
        tf: &[Isometry3<f32>],
    ) -> Self {
        let idxbuf = Self::indices(
            context,
            nslice as u32,
            contour.len() as u32,
            rep.len() as u32,
        );
        let idxcont = Self::idx_on_contour(
            context,
            nslice as u32,
            contour.len() as u32,
            rep.len() as u32,
        );
        let idxslice = Self::idx_on_slice(
            context,
            nslice as u32,
            contour.len() as u32,
            rep.len() as u32,
        );
        let idxrep = Self::idx_on_rep(
            context,
            nslice as u32,
            contour.len() as u32,
            rep.len() as u32,
        );
        Self {
            context: context.clone(),
            contour: contour.clone(),
            repetitions: rep.to_vec(),
            transforms: tf.to_vec(),
            idx_triangle: idxbuf,
            idx_contour: idxcont,
            idx_slice: idxslice,
            idx_rep: idxrep,
            first_frame: true,
            change: false,
        }
    }
    fn indices(context: &Context, nslices: u32, ncontour: u32, nrep: u32) -> Arc<ElementBuffer> {
        // todo: can deduce nslices/ncontour from self?idem in idx_on_contour
        let idx = (0..nrep)
            .flat_map(|r| {
                (0_u32..(nslices - 1))
                    .flat_map(|slice| {
                        (0_u32..ncontour)
                            .flat_map(move |p| {
                                vec![
                                    // 1st triangle
                                    p,
                                    (p + 1) % ncontour,
                                    p + ncontour,
                                    // 2nd triangle
                                    (p + 1) % ncontour,
                                    (p + 1) % ncontour + ncontour,
                                    p + ncontour,
                                ]
                            })
                            .map(move |i| i + ncontour * slice)
                    })
                    .map(move |i| i + ncontour * nslices * r)
            })
            .collect::<Vec<_>>();
        Arc::new(ElementBuffer::new_with_data(context, &idx))
    }
    fn idx_on_contour(
        context: &Context,
        nslices: u32,
        ncontour: u32,
        nrep: u32,
    ) -> Arc<VertexBuffer> {
        let idx = (0..nslices * ncontour * nrep) //todo: cycle + take instead of % op
            .map(|i| (i % ncontour) as i32)
            .collect::<Vec<_>>();
        Arc::new(VertexBuffer::new_with_data::<i32>(context, &idx))
    }
    fn idx_on_slice(
        context: &Context,
        nslices: u32,
        ncontour: u32,
        nrep: u32,
    ) -> Arc<VertexBuffer> {
        let idx = (0..nslices * ncontour)
            .map(|i| (i / ncontour) as i32) //todo: cycle take instead of div trick
            .cycle()
            .take((nslices * ncontour * nrep) as usize)
            .collect::<Vec<_>>();
        Arc::new(VertexBuffer::new_with_data::<i32>(context, &idx))
    }
    fn idx_on_rep(context: &Context, nslices: u32, ncontour: u32, nrep: u32) -> Arc<VertexBuffer> {
        let idx = (0..nrep * ncontour * nslices)
            .map(|i| (i / (ncontour * nslices)) as i32)
            .collect::<Vec<_>>();
        Arc::new(VertexBuffer::new_with_data::<i32>(context, &idx))
    }
    pub fn set_transforms(&mut self, tfs: &[Isometry3<f32>]) {
        assert_eq!(tfs.len(), self.transforms.len());
        self.transforms = tfs.to_vec();
    }
    pub fn get_transforms(self) -> Vec<Isometry3<f32>> {
        self.transforms
    }
    fn send_slice_uniforms(&self, program: &Program) {
        program.use_uniform_array(
            "slice_ts",
            self.transforms
                .iter()
                .map(|trans| trans.translation)
                .map(|t| vec3(t.x, t.y, t.z))
                .collect::<Vec<_>>()
                .as_slice(),
        );
        program.use_uniform_array(
            "slice_rs",
            self.transforms
                .iter()
                .map(|trans| trans.rotation.coords)
                .map(|q| vec4(q.x, q.y, q.z, q.w))
                .collect::<Vec<_>>()
                .as_slice(),
        );
    }
    fn draw(&self, program: &Program, render_states: RenderStates, camera: &Camera) {
        program.use_uniform("viewProjection", camera.projection() * camera.view());
        if self.first_frame {
            program.use_uniform_array(
                "contour",
                &self
                    .contour
                    .iter()
                    .map(|p| vec2(p.x, p.y))
                    .collect::<Vec<_>>(),
            );
            self.send_slice_uniforms(program);

            program.use_uniform_array("repetitions", &self.repetitions);
        }
        if self.change {
            self.send_slice_uniforms(program);
        }
        program.use_vertex_attribute("idx_cont", &self.idx_contour);
        program.use_vertex_attribute("idx_slice", &self.idx_slice);
        program.use_vertex_attribute("idx_rep", &self.idx_rep);
        program.draw_elements(render_states, camera.viewport(), &self.idx_triangle);
    }
}

impl Geometry for Tube {
    fn render_with_material(
        &self,
        material: &dyn three_d::Material,
        camera: &three_d::Camera,
        lights: &[&dyn three_d::Light],
    ) {
        let fragment_shader = material.fragment_shader(lights);
        let shader_source = format!(
            "const int T={};\nconst int C={};\nconst int R={};\n{}",
            self.transforms.len(),
            self.contour.len(),
            self.repetitions.len(),
            include_str!("shaders/tube.vert").to_owned()
        );
        self.context
            .program(shader_source, fragment_shader.source, |program| {
                material.use_uniforms(program, camera, lights);
                self.draw(program, material.render_states(), camera);
            })
            .unwrap();
    }

    fn render_with_post_material(
        &self,
        _material: &dyn three_d::PostMaterial,
        _camera: &three_d::Camera,
        _lights: &[&dyn three_d::Light],
        _color_texture: Option<three_d::ColorTexture>,
        _depth_texture: Option<three_d::DepthTexture>,
    ) {
        todo!()
    }

    fn aabb(&self) -> three_d::AxisAlignedBoundingBox {
        //todo: make it tighter
        three_d::AxisAlignedBoundingBox::INFINITE
    }
}
